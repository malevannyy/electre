import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.util.Scanner
import kotlin.math.sign

private const val SCALE = 3

@Suppress("CyclomaticComplexMethod", "kotlin:S3776")
fun main(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    println("ELECTRE I")

    val scanner = Scanner(System.`in`)

    print("Enter alternatives count: ")
    val alternativeCount = scanner.nextInt()

    print("Enter criterion count: ")
    val criteriaCount = scanner.nextInt()

    println("Enter criterion directions, ↑: + / ↓: - : ")
    val directions = IntArray(criteriaCount)
    for (j in 0 until criteriaCount) {
        directions[j] = when (scanner.next()) {
            "-" -> -1
            else -> +1
        }.sign
    }

    val a = Array(alternativeCount) { IntArray(criteriaCount) }

    println("Enter matrix values: ")
    for (i in 0 until alternativeCount) {
        for (j in 0 until criteriaCount) {
            a[i][j] = scanner.nextInt()
        }
    }

    val weights = IntArray(criteriaCount)

    println("Enter criterion weights: ")
    for (j in 0 until criteriaCount) {
        weights[j] = scanner.nextInt()
    }

    val sumWi = weights.sum()

    val lengths = IntArray(criteriaCount)
    println("Enter criterion scale lengths: ")
    for (j in 0 until criteriaCount) {
        lengths[j] = scanner.nextInt()
    }

    fun concord(i: Int, j: Int): BigDecimal {
        var r = 0
        for (k in 0 until criteriaCount) {
            r = when (a[i][k] compareTo a[j][k]) {
                directions[k].sign, 0 -> r.plus(weights[k])
                else -> r
            }
        }
        return BigDecimal(r).divide(BigDecimal(sumWi), SCALE, RoundingMode.HALF_EVEN)
    }

    val concordance = Array(alternativeCount) { Array<BigDecimal?>(alternativeCount) { null } }
    for (i in 0 until alternativeCount) {
        for (j in 0 until alternativeCount) {
            concordance[i][j] = when (i == j) {
                true -> null
                else -> concord(i, j)
            }
        }
    }

    fun discord(i: Int, j: Int) = a[i].zip(a[j]) { a, b -> b - a }
        .mapIndexed { k, delta ->
            BigDecimal(delta * directions[k]).divide(BigDecimal(lengths[k]), SCALE, RoundingMode.HALF_EVEN)
        }
        .max().takeIf { it > ZERO }
        ?: ZERO

    val discordance = Array(alternativeCount) { Array<BigDecimal?>(alternativeCount) { null } }
    for (i in 0 until alternativeCount) {
        for (j in 0 until alternativeCount) {
            discordance[i][j] = when (i == j) {
                true -> null
                else -> discord(i, j)
            }
        }
    }

    println("Concordance:")
    println(concordance.toMatrix())

    println("Discordance:")
    println(discordance.toMatrix())
}

const val PLACE_HOLDER = "*"

fun <T> Array<Array<T?>>.toMatrix() = joinToString("\n") { row ->
    row.joinToString("\t") { it?.toString() ?: PLACE_HOLDER }
}





